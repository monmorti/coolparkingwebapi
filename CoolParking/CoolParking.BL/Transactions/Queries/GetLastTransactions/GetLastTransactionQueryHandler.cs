﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using MediatR;

namespace CoolParking.BL.Transactions.Queries.GetLastTransactions
{
    public class GetLastTransactionQueryHandler : IRequestHandler<GetLastTransactionsQuery, IList<TransactionViewModel>>
    {
        private readonly IParkingService _parkingService;
        private readonly IMapper _mapper;

        public GetLastTransactionQueryHandler(IParkingService parkingService, IMapper mapper)
        {
            _parkingService = parkingService;
            _mapper = mapper;
        }

        public Task<IList<TransactionViewModel>> Handle(GetLastTransactionsQuery request, CancellationToken cancellationToken)
        {
            var transactions = _parkingService.GetLastParkingTransactions();
            var result = _mapper.Map<TransactionInfo[], IList<TransactionViewModel>>(transactions);


            return Task.FromResult(result);

        }
    }
}