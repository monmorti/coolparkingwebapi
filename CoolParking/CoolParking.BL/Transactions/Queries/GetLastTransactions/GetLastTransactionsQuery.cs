﻿using System.Collections.Generic;
using MediatR;

namespace CoolParking.BL.Transactions.Queries.GetLastTransactions
{
    public class GetLastTransactionsQuery : IRequest<IList<TransactionViewModel>> { }
}