﻿using System;
using CoolParking.BL.Interfaces.Mapping;
using CoolParking.BL.Models;

namespace CoolParking.BL.Transactions.Queries.GetLastTransactions
{
    public class TransactionViewModel : IMapFrom<TransactionInfo>
    {
        public string VehicleId { get; set; }
        public string Sum { get; set; }
        public DateTime TransactionDate { get; set; }
    }
}