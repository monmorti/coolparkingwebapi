﻿using MediatR;

namespace CoolParking.BL.Transactions.Queries.GetAllTransactions
{
    public class GetAllTransactionsQuery:IRequest<GetAllTransactionsQueryResult>
    {
        
    }
}