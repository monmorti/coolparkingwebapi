﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using MediatR;

namespace CoolParking.BL.Transactions.Queries.GetAllTransactions
{
    public class GetAllTransactionsQueryHandler : IRequestHandler<GetAllTransactionsQuery, GetAllTransactionsQueryResult>
    {
        private readonly IParkingService _parkingService;
        public GetAllTransactionsQueryHandler(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        public Task<GetAllTransactionsQueryResult> Handle(GetAllTransactionsQuery request, CancellationToken cancellationToken)
        {
            var result = new GetAllTransactionsQueryResult();
            try
            {
                result.Value = _parkingService.ReadFromLog();
                result.Success = true;
            }
            catch (InvalidOperationException ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            return Task.FromResult(result);
        }
    }
}