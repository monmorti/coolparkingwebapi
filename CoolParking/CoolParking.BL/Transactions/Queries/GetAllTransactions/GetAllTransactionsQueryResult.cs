﻿namespace CoolParking.BL.Transactions.Queries.GetAllTransactions
{
    public class GetAllTransactionsQueryResult
    {
        public bool Success { get; set; }
        public string Value { get; set; }
        public string Message { get; set; }
    }
}