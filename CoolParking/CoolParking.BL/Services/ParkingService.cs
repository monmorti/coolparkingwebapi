﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.


using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;

        private readonly Parking _parking;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _parking = Parking.Instance;

            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Elapsed += Withdraw;
            _withdrawTimer.Interval = Settings.LogInterval;
            _withdrawTimer.Start();


            _logTimer = logTimer;
            _logTimer.Elapsed += LogTransactionInfos;
            _logTimer.Interval = Settings.LogInterval;
            _logTimer.Start();

            _logService = logService;
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.Capacity - _parking.Vehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            // TODO: Can I change values of Vehicle? If so then fix
            return new ReadOnlyCollection<Vehicle>(_parking.Vehicles.Values.ToList());
        }

        public void AddVehicle(Vehicle vehicle)
        {

            if (GetFreePlaces() == 0)
                throw new InvalidOperationException("Parking doesn't have free spots!");


            if (GetFreePlaces() > 0)
            {
                // TODO: Validate 
                // TODO: Throw id exist
                _parking.Vehicles.Add(vehicle.Id, vehicle);
            }
        }


        private Vehicle FindVehicle(string vehicleId)
        {
            if (String.IsNullOrWhiteSpace(vehicleId))
                throw new ArgumentException($"Invalid {nameof(vehicleId)}: {vehicleId}");

            var isVehicleExist = _parking.Vehicles.TryGetValue(vehicleId, out Vehicle vehicle);

            if (!isVehicleExist)
                throw new ArgumentException($"Parking doesn't have car! Plate: {vehicleId}");


            return vehicle;
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = FindVehicle(vehicleId);

            if (vehicle.Balance < 0)
                throw new InvalidOperationException($"Vehicle with dept cannot be removed from parking! Value to be payed: {vehicle.Balance}");

            _parking.Vehicles.Remove(vehicleId);
        }

        private void Withdraw(object sender, ElapsedEventArgs args)
        {
            foreach (Vehicle vehicle in _parking.Vehicles.Values)
            {
                decimal rate = FindRate(vehicle.VehicleType);
                decimal bill = CalculatePaymentAmount(vehicle.Balance, rate);

                vehicle.Balance -= bill;
                _parking.Balance += bill;

                SaveTransaction(vehicle.Id, bill);
            }
        }

        private decimal CalculatePaymentAmount(decimal balance, decimal rate)
        {
            decimal bill;
            bool hasEnoughMoney = balance >= rate;
            if (hasEnoughMoney)
                bill = rate;
            else if (balance > 0)
                bill = balance + (rate-balance) * Settings.ParkingRatePenalty;
            else
                bill = rate * Settings.ParkingRatePenalty;
            return bill;
        }

        private void SaveTransaction(string vehicleId, decimal sum)
        {
            _parking.TransactionInfos.Add(new TransactionInfo(vehicleId, sum, DateTime.UtcNow));
        }

        private decimal FindRate(VehicleType vehicleType)
        {
            bool hasSpecificRate = Settings.ParkingRate.TryGetValue(vehicleType, out var rate);
            if (!hasSpecificRate)
                rate = Settings.ParkingRateDefault;

            return rate;
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum <= 0)
                throw new ArgumentException($"Negative sum value! Sum = {sum}");

            var vehicle = FindVehicle(vehicleId);
            vehicle.Balance += sum;
        }

        private void LogTransactionInfos(object sender, ElapsedEventArgs args)
        {
            var transactions =  _parking.TransactionInfos.ToArray().OrderByDescending(x=>x.TransactionTime);
            _parking.TransactionInfos.Clear();


            StringBuilder log = new StringBuilder();
            log.AppendLine(new string('*', 50));
            foreach (TransactionInfo transactionInfo in transactions)
            {
                log.AppendLine($"{transactionInfo.TransactionTime.ToUniversalTime()}: ${transactionInfo.Id} - withdrew ${transactionInfo.Sum}");
            }

            _logService.Write(log.ToString());
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _parking.TransactionInfos.ToArray();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void Dispose()
        {

            // TODO:
            // What if we have two instances of ParkingService and one of them after a while was disposed
            // Collection will be cleared and other service will get unexpected behavior 
            // Possible fix: check the number of instances and when last of them was disposed clear collection
            _parking?.Dispose();
            _withdrawTimer?.Dispose();
            _logTimer?.Dispose();
        }
    }
}