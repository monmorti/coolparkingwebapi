﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService(string path)
        {
            // TODO: Validate? 
            LogPath = path;
        }

        public string LogPath { get; }
        public void Write(string logInfo)
        {
            using (var writer = new StreamWriter(LogPath,true))
            {
                writer.WriteLine(logInfo);
                writer.Flush();
            }
        }

        public string Read()
        {
            if (!File.Exists(LogPath))
            {
                throw new InvalidOperationException($"Log File path doesn't exist! Path: ${LogPath}");
            }

            return File.ReadAllText(LogPath);
        }
    }
}