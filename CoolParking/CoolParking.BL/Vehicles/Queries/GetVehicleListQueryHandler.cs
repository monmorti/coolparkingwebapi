﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using MediatR;

namespace CoolParking.BL.Vehicles.Queries
{
    public class GetVehicleListQueryHandler : IRequestHandler<GetVehicleListQuery, IList<VehicleViewModel>>
    {
        private readonly IParkingService _parkingService;
        private readonly IMapper _mapper;

        public GetVehicleListQueryHandler(IParkingService parkingService, IMapper mapper)
        {
            _parkingService = parkingService;
            _mapper = mapper;
        }

        public Task<IList<VehicleViewModel>> Handle(GetVehicleListQuery request, CancellationToken cancellationToken)
        {
            var vehicles = _parkingService.GetVehicles();
            IList<VehicleViewModel> result = _mapper.Map<IReadOnlyCollection<Vehicle>, IList<VehicleViewModel>>(vehicles);

            return Task.FromResult(result);
        }
    }
}