﻿using System.Collections.Generic;
using MediatR;

namespace CoolParking.BL.Vehicles.Queries
{
    public class GetVehicleListQuery : IRequest<IList<VehicleViewModel>> { }
}