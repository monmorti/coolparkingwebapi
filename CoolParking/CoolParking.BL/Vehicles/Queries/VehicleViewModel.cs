﻿using CoolParking.BL.Interfaces.Mapping;
using CoolParking.BL.Models;

namespace CoolParking.BL.Vehicles.Queries
{
    public class VehicleViewModel:IMapFrom<Vehicle>
    {
        public string Id { get; set; }
        public int VehicleType { get; set; }
        public decimal Balance { get; set; }
    }
}