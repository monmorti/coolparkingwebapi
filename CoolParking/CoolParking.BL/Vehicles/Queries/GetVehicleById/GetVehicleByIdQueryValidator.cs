﻿using CoolParking.BL.Models;
using FluentValidation;

namespace CoolParking.BL.Vehicles.Queries.GetVehicleById
{
    public class GetVehicleByIdQueryValidator : AbstractValidator<GetVehicleByIdQuery>
    {
        public GetVehicleByIdQueryValidator()
        {
            RuleFor(x => x.Id).Must(Vehicle.IsValidatePlateFormat)
                .WithMessage("vehicle id should have format “AA-0001-AA”");
        }
    }
}