﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using MediatR;

namespace CoolParking.BL.Vehicles.Queries.GetVehicleById
{
    public class GetVehicleByIdQueryHandler:IRequestHandler<GetVehicleByIdQuery, VehicleViewModel>
    {
        private readonly IParkingService _parking;
        private readonly IMapper _mapper;

        public GetVehicleByIdQueryHandler(IParkingService parking, IMapper mapper)
        {
            _parking = parking;
            _mapper = mapper;
        }

        public Task<VehicleViewModel> Handle(GetVehicleByIdQuery request, CancellationToken cancellationToken)
        {
            var vehicle = _parking.GetVehicles().FirstOrDefault(x => x.Id == request.Id);
            var result = _mapper.Map<Vehicle, VehicleViewModel>(vehicle);
            return Task.FromResult(result);
        }
    }
}