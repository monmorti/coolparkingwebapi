﻿using MediatR;

namespace CoolParking.BL.Vehicles.Queries.GetVehicleById
{
    public class GetVehicleByIdQuery : IRequest<VehicleViewModel>
    {
        public string Id { get; set; }
    }
}