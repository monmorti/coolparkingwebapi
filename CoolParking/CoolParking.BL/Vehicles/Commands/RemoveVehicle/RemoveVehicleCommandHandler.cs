﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using MediatR;

namespace CoolParking.BL.Vehicles.Commands.RemoveVehicle
{
    public class RemoveVehicleCommandHandler : IRequestHandler<RemoveVehicleCommand, RemoveVehicleCommandResult>
    {
        private readonly IParkingService _parkingService;

        public RemoveVehicleCommandHandler(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public Task<RemoveVehicleCommandResult> Handle(RemoveVehicleCommand request, CancellationToken cancellationToken)
        {
            RemoveVehicleCommandResult result = new RemoveVehicleCommandResult();
            try
            {
                _parkingService.RemoveVehicle(request.Id);
                result.Success = true;
            }
            catch (ArgumentException ex)
            {
                result.ErrorMessage = ex.Message;
                result.StatusCode = 400;
                result.Success = false;
            }

            return Task.FromResult(result);
        }
    }
}