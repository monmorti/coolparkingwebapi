﻿namespace CoolParking.BL.Vehicles.Commands.RemoveVehicle
{
    public class RemoveVehicleCommandResult
    {
        public bool Success { get; set; }
        // TODO: Change on more descriptive model
        public string ErrorMessage { get; set; }
        public int StatusCode { get; set; }
    }
}