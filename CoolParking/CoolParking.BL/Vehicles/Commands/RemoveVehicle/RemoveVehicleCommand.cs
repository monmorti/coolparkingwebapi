﻿using MediatR;

namespace CoolParking.BL.Vehicles.Commands.RemoveVehicle
{
    public class RemoveVehicleCommand : IRequest<RemoveVehicleCommandResult>
    {
        public string Id { get; set; }
    }
}