﻿using CoolParking.BL.Vehicles.Queries;

namespace CoolParking.BL.Vehicles.Commands.AddVehicle
{

    // TODO: To Generic Model
    public class AddVehicleCommandResult
    {
        public bool Success { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public VehicleViewModel Value { get; set; }
    }
}