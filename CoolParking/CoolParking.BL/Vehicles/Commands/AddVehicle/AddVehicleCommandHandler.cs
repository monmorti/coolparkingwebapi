﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Vehicles.Queries;
using MediatR;

namespace CoolParking.BL.Vehicles.Commands.AddVehicle
{
    public class AddVehicleCommandHandler : IRequestHandler<AddVehicleCommand, AddVehicleCommandResult>
    {
        private readonly IParkingService _parkingService;
        private readonly IMapper _mapper;

        public AddVehicleCommandHandler(IParkingService parkingService, IMapper mapper)
        {
            _parkingService = parkingService;
            _mapper = mapper;
        }

        public Task<AddVehicleCommandResult> Handle(AddVehicleCommand request, CancellationToken cancellationToken)
        {
            var result = new AddVehicleCommandResult();
            var newVehicle = new Vehicle(request.Id, (VehicleType)request.VehicleType, request.Balance);

            try
            {

                _parkingService.AddVehicle(newVehicle);
                // in case that parking service changes fields
                var parkedVehicle = _parkingService.GetVehicles().FirstOrDefault(x => x.Id == request.Id);

                var vm = _mapper.Map<Vehicle, VehicleViewModel>(parkedVehicle);
                result.Success = true;
                result.Value = vm;
            }
            catch (ArgumentException ex)
            {
                result.Success = false;
                result.StatusCode = 400;
                result.Message = ex.Message;
            }





            return Task.FromResult(result);
        }
    }
}