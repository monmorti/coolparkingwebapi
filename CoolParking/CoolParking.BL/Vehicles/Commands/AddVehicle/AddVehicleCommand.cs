﻿using MediatR;

namespace CoolParking.BL.Vehicles.Commands.AddVehicle
{
    public class AddVehicleCommand : IRequest<AddVehicleCommandResult>
    {
        public string Id { get; set; }
        public int VehicleType { get; set; }
        public decimal Balance { get; set; }
    }
}   