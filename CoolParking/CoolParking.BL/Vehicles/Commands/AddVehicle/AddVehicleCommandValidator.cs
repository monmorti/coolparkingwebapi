﻿using System;
using CoolParking.BL.Models;
using FluentValidation;

namespace CoolParking.BL.Vehicles.Commands.AddVehicle
{
    public class AddVehicleCommandValidator : AbstractValidator<AddVehicleCommand>
    {
        public AddVehicleCommandValidator()
        {
            RuleFor(x => x.Id).NotEmpty().Must(Vehicle.IsValidatePlateFormat).WithMessage("vehicle id should have format “AA-0001-AA”");
            RuleFor(x => x.Balance).Must(x => x >= 0).WithMessage("Vehicle balance could not be negative!");
            RuleFor(x => x.VehicleType).Must(x => Enum.IsDefined(typeof(VehicleType), x))
                .WithMessage("Vehicle with such type does not exist!");
        }
    }
}