﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public TransactionInfo(string id, decimal sum, DateTime time)
        {
            Id = id;
            Sum = sum;
            TransactionTime = time;
        }
        public string Id { get; }
        public decimal Sum { get; }
        public DateTime TransactionTime { get; }
    }
}