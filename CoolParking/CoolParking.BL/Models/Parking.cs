﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.


using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public sealed class Parking : IDisposable
    {
        public Dictionary<string, Vehicle> Vehicles { get; }
        public decimal Balance { get; internal set; }
        public int Capacity { get; }
        public List<TransactionInfo> TransactionInfos { get; } 


        public static Parking Instance { get; } = new Parking();
        static Parking() { }

        private Parking()
        {
            Balance = Settings.InitialParkingBalance;
            Capacity = Settings.ParkingCapacity;
            Vehicles = new Dictionary<string, Vehicle>();
            TransactionInfos = new List<TransactionInfo>();
        }

        public void Dispose()
        {
            TransactionInfos.Clear();
            Balance = 0;
            Vehicles.Clear();
        }
    }
}