﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static int ParkingCapacity { get; } = 10;
        public static decimal InitialParkingBalance { get; } = 0;

        public static Dictionary<VehicleType, decimal> ParkingRate =
            new Dictionary<VehicleType, decimal>
            {
                {VehicleType.PassengerCar, 2},
                {VehicleType.Truck,5},
                {VehicleType.Bus, 3.5m},
                {VehicleType.Motorcycle, 1m}
            };

        public static decimal ParkingRateDefault = 5;
        public static decimal ParkingRatePenalty { get; } = 2.5m;
        public static int LogInterval { get; set; } = 60 * 1000;
        public static int WithdrawInterval { get; set; } = 5 * 1000;
    }
}