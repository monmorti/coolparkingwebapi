﻿using System.IO;
using System.Reflection;
using AutoMapper;
using CoolParking.BL.Infrastructure.AutoMapper;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace CoolParking.BL
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddAutoMapper(new Assembly[] { typeof(AutoMapperProfile).GetTypeInfo().Assembly });
            services.AddSingleton<ILogService>(x => new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log"));
            services.AddSingleton<ITimerService, TimerService>();
            services.AddSingleton<IParkingService, ParkingService>();
            services.AddMediatR(Assembly.GetExecutingAssembly());

            return services;
        }
    }
}