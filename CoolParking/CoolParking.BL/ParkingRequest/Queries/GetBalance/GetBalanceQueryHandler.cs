﻿using System.Threading;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using MediatR;

namespace CoolParking.BL.ParkingRequest.Queries.GetBalance
{
    public class GetBalanceQueryHandler : IRequestHandler<GetBalanceQuery, decimal>
    {
        private readonly IParkingService _parkingService;
        public GetBalanceQueryHandler(IParkingService parkingService) => _parkingService = parkingService;

        public Task<decimal> Handle(GetBalanceQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_parkingService.GetBalance());
        }
    }
}