﻿using MediatR;

namespace CoolParking.BL.ParkingRequest.Queries.GetBalance
{
    public class GetBalanceQuery : IRequest<decimal> { }
}