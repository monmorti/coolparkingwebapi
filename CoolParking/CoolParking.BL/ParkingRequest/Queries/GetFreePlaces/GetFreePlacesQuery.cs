﻿using MediatR;

namespace CoolParking.BL.ParkingRequest.Queries.GetFreePlaces
{
    public class GetFreePlacesQuery : IRequest<int> { }
}