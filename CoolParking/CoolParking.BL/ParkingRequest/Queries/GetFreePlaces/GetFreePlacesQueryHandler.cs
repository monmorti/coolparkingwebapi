﻿using System.Threading;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using MediatR;

namespace CoolParking.BL.ParkingRequest.Queries.GetFreePlaces
{
    public class GetFreePlacesQueryHandler : IRequestHandler<GetFreePlacesQuery, int>
    {
        private readonly IParkingService _parkingService;

        public GetFreePlacesQueryHandler(IParkingService parkingService) => _parkingService = parkingService;

        public Task<int> Handle(GetFreePlacesQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_parkingService.GetFreePlaces());
        }
    }
}