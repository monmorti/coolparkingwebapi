﻿using AutoMapper;

namespace CoolParking.BL.Interfaces.Mapping
{
    public interface IHaveCustomMapping
    {
        void CreateMappings(Profile configuration);
    }
}