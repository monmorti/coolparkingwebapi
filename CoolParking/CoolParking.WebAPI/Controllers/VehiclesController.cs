﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CoolParking.BL.Vehicles.Commands.AddVehicle;
using CoolParking.BL.Vehicles.Commands.RemoveVehicle;
using CoolParking.BL.Vehicles.Queries;
using CoolParking.BL.Vehicles.Queries.GetVehicleById;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/{controller}")]
    [Produces("application/json")]
    public class VehiclesController : BaseController
    {
        [HttpGet]
        [ActionName("index")]
        public async Task<ActionResult<IList<VehicleViewModel>>> GetAll()
        {
            var result = await Mediator.Send(new GetVehicleListQuery());
            return Ok(result);
        }

        [HttpGet("{id}")]
        [Route("api/vehicles/{id}")]
        public async Task<ActionResult<VehicleViewModel>> GetById([FromRoute] GetVehicleByIdQuery query)
        {
            var result = await Mediator.Send(query);
            
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpPost]
        [ActionName("Index")]
        public async Task<ActionResult<VehicleViewModel>> AddVehicle([FromBody] AddVehicleCommand command)
        {
            var result = await Mediator.Send(command);
            return Ok(result);
        }


        [HttpDelete("{id}")]
        [Route("api/vehicles/{id}")]
        public async Task<IActionResult> Remove([FromRoute] RemoveVehicleCommand command)
        {
            var result = await Mediator.Send(command);
            if (!result.Success)
            {
                // TODO: Create ApiErrorModel, and each error should be the same format
                return BadRequest(result);
            }

            return NoContent();
        }
    }
}