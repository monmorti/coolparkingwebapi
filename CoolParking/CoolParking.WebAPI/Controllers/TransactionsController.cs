﻿using System;
using System.Threading.Tasks;
using CoolParking.BL.Transactions.Queries.GetAllTransactions;
using CoolParking.BL.Transactions.Queries.GetLastTransactions;
using CoolParking.BL.Vehicles.Queries;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/{controller}/{action}")]
    public class TransactionsController : BaseController
    {
        [HttpGet]
        public async Task<IActionResult> Last()
        {
            var result = await Mediator.Send(new GetLastTransactionsQuery());
            return Ok(result);
        }

        [HttpGet]
        public async Task<ActionResult<string>> All()
        {
            var result = await Mediator.Send(new GetAllTransactionsQuery());

            if (!result.Success)
            {
                return NotFound(new { result.Message });
            }

            return Ok(result.Value);
        }

        [HttpPut]
        public async Task<VehicleViewModel> TopUpVehicle()
        {
            throw new NotImplementedException();
        }

    }
}