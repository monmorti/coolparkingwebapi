﻿using System.Threading.Tasks;
using CoolParking.BL.ParkingRequest.Queries.GetBalance;
using CoolParking.BL.ParkingRequest.Queries.GetFreePlaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/{controller}/{action}")]
    public class ParkingController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult<decimal>> Balance()
        {
            var result = await Mediator.Send(new GetBalanceQuery());

            return Ok(result);
        }

        [HttpGet]
        public async Task<ActionResult<int>> FreePlaces()
        {
            var result = await Mediator.Send(new GetFreePlacesQuery());
            return Ok(result);
        }
    }
}
