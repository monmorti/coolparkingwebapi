﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using ConsoleTables;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.ConsoleClient
{

    // TODO: Dependency resolver
    class Program
    {
        private static readonly string _logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

        private static readonly ILogService _logService = new LogService(_logFilePath);
        private static readonly ITimerService _withdrawTimer = new TimerService();
        private static readonly ITimerService _logTimer = new TimerService();
        private static readonly IParkingService _parking = new ParkingService(_withdrawTimer, _logTimer, _logService);
        static void Main(string[] args)
        {
            //Task.Run(() =>
            //{
            //    while (!(Console.KeyAvailable && Console.ReadKey(true).Key == ConsoleKey.Escape))
            //    {
            // Terminate process on 
            //    }

            //});







            var exit = true;
            while (exit)
            {
                PrintOptions();
                var keyPressed = Console.ReadKey();
                Console.Clear();

                if (keyPressed.KeyChar == '0')
                    exit = false;
                else
                    HandleAction(keyPressed.KeyChar);
            }

            Dispose();
        }

        static void Dispose()
        {
            _parking.Dispose();
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
        }

        private static void HandleAction(char actionChar)
        {
            switch (actionChar)
            {
                case '1':
                    {
                        ShowBalance();
                        break;
                    }

                case '2':
                    {

                        ShowSum();
                        break;
                    }

                case '3':
                    {
                        ShowFreeSpots();
                        break;
                    }

                case '4':
                    {
                        ShowTransactions();
                        break;
                    }

                case '5':
                    {
                        ShowTransactionHistory();
                        break;
                    }

                case '6':
                    {
                        ShowVehicleList();
                        break;
                    }

                case '7':
                    {
                        ParkVehicle();
                        break;
                    }

                case '8':
                {
                    RemoveVehicle();
                    break;
                }

                case '9':
                {
                    ReplenishVehicle();
                    break;
                }

                default:
                    {
                        DefaultHandler();
                        break;
                    }
            }
        }

        private static string AskPlate()
        {
            Console.WriteLine($"Enter license plate (Example: {Vehicle.GenerateRandomRegistrationPlateNumber()} ):");
            var plate = Console.ReadLine();
            return plate;
        }

        private static void ReplenishVehicle()
        {
            Console.WriteLine("Replenish vehicle balance:");

            try
            {

                var plate = AskPlate();
                Console.WriteLine("Enter amount:");
                var amout = decimal.Parse(Console.ReadLine());
                _parking.TopUpVehicle(plate, amout);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }


            Wait();
        }

        private static void RemoveVehicle()
        {
            Console.WriteLine("Remove car!");
            try
            {
                var plate = AskPlate();

                _parking.RemoveVehicle(plate);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Success!");
            Wait();
        }



        private static void ParkVehicle()
        {
            try
            {
                Console.WriteLine($"Enter license plate (Example: {Vehicle.GenerateRandomRegistrationPlateNumber()} ):");
                var plate = Console.ReadLine();


                Console.WriteLine($"Enter vehicle type number ({nameof(VehicleType.PassengerCar)}-{(int)VehicleType.PassengerCar}; {nameof(VehicleType.Bus)}-{(int)VehicleType.Bus}; {nameof(VehicleType.Truck)}-{(int)VehicleType.Truck}; {nameof(VehicleType.Motorcycle)}-{(int)VehicleType.Motorcycle};): ");
                var type = int.Parse(Console.ReadLine());

                Console.WriteLine("Enter vehicle balance:");
                var balance = decimal.Parse(Console.ReadLine());

                _parking.AddVehicle(new Vehicle(plate, (VehicleType)type, balance));
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
            }
            Console.WriteLine("Success!");
            Wait();
        }

        public static void ShowVehicleList()
        {
            //_parking.AddVehicle(new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(),VehicleType.Bus,22));
            var vehicles = _parking.GetVehicles();
            var table = new ConsoleTable("Id", "Type", "Balance");

            foreach (Vehicle vehicle in vehicles)
            {
                table.AddRow(vehicle.Id, vehicle.VehicleType, vehicle.Balance);
            }
            table.Write();
            Console.WriteLine();
            Wait();
        }

        public static void ShowTransactionHistory()
        {
            try
            {
                var log = _parking.ReadFromLog();
                Console.WriteLine(log);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Wait();
        }

        private static void Wait()
        {
            Console.WriteLine("Press Enter to return to menu");
            Console.ReadLine();
            Console.Clear();
        }

        private static void ShowTransactions()
        {

            var transactions = _parking.GetLastParkingTransactions();
            var table = new ConsoleTable("Id", "Sum", "Date");

            foreach (TransactionInfo transaction in transactions)
            {
                table.AddRow(transaction.Id, transaction.Sum, transaction.TransactionTime);
            }
            table.Write();
            Console.WriteLine();
            Wait();
        }

        private static void DefaultHandler()
        {
            Console.WriteLine("This option doesn't exist or not implemented");
            Wait();
        }

        private static void ShowBalance()
        {
            Console.WriteLine($"Parking balance: ${_parking.GetBalance()}");
            Wait();
        }

        private static void ShowSum()
        {
            Console.WriteLine("NOt implemented yet");
            Wait();
        }

        private static void ShowFreeSpots()
        {
            Console.WriteLine($"Parking Capacity: {_parking.GetCapacity()}");
            Console.WriteLine($"Parking Free Spots: {_parking.GetFreePlaces()}");
            Wait();
        }

        private static void PrintOptions()
        {
            Console.WriteLine("1. Show balance");
            Console.WriteLine("2. Show sum");
            Console.WriteLine("3. Show parking capacity and free spots");
            Console.WriteLine("4. Show transactions of current period");
            Console.WriteLine("5. Show transactions history");
            Console.WriteLine("6. Show vehicles list presented in parking");
            Console.WriteLine("7. Park vehicle");
            Console.WriteLine("8. Remove vehicle from parking");
            Console.WriteLine("9. Replenish vehicle balance");
            Console.WriteLine("0. Exit");
        }
    }
}
