﻿using System.Threading.Tasks;
using CoolParking.WebAPI;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace CoolParking.WebApi.FunctionalTests.Controllers.Parking
{
    public class GetBalance : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;

        public GetBalance(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task ReturnsSuccessResult()
        {
            var client = _factory.CreateClient();
            
            var response = await client.GetAsync("api/parking/balance");

            response.EnsureSuccessStatusCode();
        }
    }
}