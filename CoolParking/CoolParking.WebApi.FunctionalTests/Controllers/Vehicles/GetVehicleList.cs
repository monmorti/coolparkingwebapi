﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Vehicles.Queries;
using CoolParking.WebAPI;
using CoolParking.WebApi.FunctionalTests.Common;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace CoolParking.WebApi.FunctionalTests.Controllers.Vehicles
{
    public class GetVehicleList:IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;
        public GetVehicleList(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }




        [Fact]
        public async Task ReturnsSuccessResult()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("api/vehicles");

            response.EnsureSuccessStatusCode();

            var vm = await response.GetResponseContent<List<VehicleViewModel>>();
            Assert.IsType<List<VehicleViewModel>>(vm);
            Assert.NotEmpty(vm);
        }
    }
}