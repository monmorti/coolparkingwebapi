﻿using System.Net.Http;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;

namespace CoolParking.WebApi.FunctionalTests.Common
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder
                .ConfigureServices(services =>
                {
                    services.AddTransient<IParkingService, FakeParkingService>();

                })
                .UseEnvironment("Test");
        }
    }
}