﻿using System;
using System.Collections.ObjectModel;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.WebApi.FunctionalTests.Common
{
    public class FakeParkingService : IParkingService
    {
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public decimal GetBalance()
        {
            throw new NotImplementedException();
        }

        public int GetCapacity()
        {
            throw new NotImplementedException();
        }

        public int GetFreePlaces()
        {
            throw new NotImplementedException();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            throw new NotImplementedException();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            throw new NotImplementedException();
        }

        public void RemoveVehicle(string vehicleId)
        {
            throw new NotImplementedException();
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            throw new NotImplementedException();
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            throw new NotImplementedException();
        }

        public string ReadFromLog()
        {
            throw new NotImplementedException();
        }
    }
}